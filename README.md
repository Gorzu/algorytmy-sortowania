# README #

Program umożliwia sortowanie zadanej liczby elementów 100 tablic w krótkim czasie, a także porównanie różnych algorytmów sortowania. Całość wyposażona jest w wygodne menu.

### Zawarte algorytmy ###

* Sortowanie szybkie (Quicksort)
* Sortowanie przez scalanie (Mergesort)
* Sortowanie introspektywne (Introsort)
* Sortowanie przez kopcowanie (Heapsort) - składowa sortowania introspektywnego
* Sortowanie przez wstawianie (Insertionsort) - składowa sortowania introspektywnego

### Wynik testów ###

Wyniki mogą nieco się różnić w zależności od mocy obliczeniowej komputera. 
Jednak w każdym wypadku początkowego ułożenia i wielkości danych, najskuteczniejszym algorytmem sortowania jest sortowanie introspektywne.

### Kto jest autorem? ###

* Właściciel repozytorium Piotr Gorzelnik