#include "Sorts.hh"

using namespace std;
using namespace std::chrono;

///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Bazowe funkcje ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

/* wypisuje tablice */
template<typename T>
void WyswietlStan(T data, int length)
{
	cout << "\n Stan tablicy: \n";

	for(int j=0; j<ROZMIAR; j++)  // utworzenie 100 pustych tablic 
	{
		cout << "Tablica numer: " << j+1 << ". " << endl;
		for(int i=0; i<length; i++)    // wypełnienie tablic
		{
			cout << i+1 << ". " << data[j][i] << endl;
		}
		cout << endl;
	}
}

/* Prosta podmiana wartości */

template<typename T>
void swap(T *a, T *b)
{
	T *temp = a;
	a = b;
	b = *temp;
}


/* Stworzenie tablic z zupełnie losowymi elementami */
template<typename T>
void FillRandomly(T data, int length)
{
    srand(time(NULL));

    for(int j =0; j < ROZMIAR; j++)
    {
        for(int i = 0; i <length; i++)
        {
            data[j][i] = rand();
        }
    }
}



/* Stworzenie tablicy posortowaniej malejąco */
template<typename T>
void FillReverse(T *data, int length)
{
    for (int begin = 0, end = length - 1; begin < end; begin++, end--)
		swap(data[begin], data[end]);
}



/* Wstępne posortowanie części tablicy */
template<typename T>
void SortPercent(T data, int length, double percent)
{
 	srand(time(NULL));

    int LengthToSort = percent * length;
  	//QuickSort(data, 0, LengthToSort-1); //posortowanie zadanego procenta

    for(int i=0; i<LengthToSort; ++i)
    	data[i] = i;

    for(int i=LengthToSort+1; i<length; ++i)
    	data[i] = rand();

}

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// QUICKSORT /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
void QuickSort(T data[], int left, int right)
{
	
	int i, j, tmp, piwot;

	i = (left+right)/2;
	piwot = data[i];
	data[i] = data[left];

	for(j = i = left; i < right; i++)
		if(data[i] < piwot)
		{
			tmp = data[i];
			data[i] = data[j];
			data[j] = tmp;
			j++;
		}
	


	data[right] = data[j];
	data[j] = piwot;

	if(left < j-1)
		QuickSort(data, left, j-1);

	if(j+1 < right)
		QuickSort(data, j+1, right);

}

///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// MERGESORT ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
template<typename T>
void Merge(T data[], T NewArray[], int left, int middle, int right) 
{
	for(int i = left; i <= right; i++)
	{ 
		NewArray[i] = data[i];
	 }  
	
	int p1, p2, c;  // wskazniki tablic (pointer1, pointer2, current)
	p1 = c = left;        
	p2 = middle + 1;

	while(p1 <= middle && p2 <= right)
	{
		if(NewArray[p1] <= NewArray[p2]) 
		{
			data[c++] = NewArray[p1++];
		}
		
		else
		{
			data[c++] = NewArray[p2++];
		}
	}

	while (p1 <= middle)
	{
		data[c++] = NewArray[p1++];
	}

}


template<typename T>
void MergeSort(T data[], T NewArray[], int left, int right)
{
	int middle;

	if(left < right)     // podzial duzej tablicy na wiele malych tablic
	{
		middle = (left + right) / 2;
		MergeSort(data, NewArray, left, middle);       // dzielenie lewej strony
		MergeSort(data, NewArray, middle+1, right);    // dzielenie prawej strony
		Merge(data, NewArray, left, middle, right);    // scalanie obu stron
	}
}



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// HEAPROSORT ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
// sortowanie przez kopcowanie jest potrzebne do sortowania introspektywnego

template<typename T>
void ConvertHeap(T data[], int length, int x)
{
	int max = x;
	int left = 2*x+1;
	int right = 2*x+2;

	if (left < length && data[max] < data[left])
		max = left;

	if(right < length && data[max] < data[left])
		max = right;

	if(max != x)
	{
		swap(data[x], data[max]);
		ConvertHeap(data, length, max);
	}

}

template<typename T>
void HeapSort(T data[], int length)
{
	for(int i=length/2 - 1; i >= 0; i--)
		ConvertHeap(data, length, i);

	for (int i = length-1; i >= 0; i--)
	{
		swap(data[0], data[i]);
		ConvertHeap(data, i, 0);
	}
}


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// INSERTIONOSORT ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
// sortowanie przez wstawianie jest potrzebne do sortowania introspektywnego

template<typename T>
void InsertionSort(T *tab, int left, int right)
{
	for (int i = left + 1; i <= right; i++)
	{
		int key = tab[i];
		int j = i;

		while (j > left && tab[j-1] > key)
        {
            tab[j] = tab[j-1];
            j--;
        }
		tab[j] = key;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// INTROSORT ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


template<typename T>
int Partition(T data[], int left, int right)
{
    T tmp;
    int pivot = left + (right-left)/2;   
    
    T Value = data[pivot];
    tmp = data[pivot];
    data[pivot] = data[right];
    data[right] = tmp;
    int border = left - 1;
    int i = left;

    while(i < right)
    {
        if(data[i] < Value)
        {
            border++;
            if(border != i)
            {   // bez funkcji swap() ponieważ znacząco spowalniała cały proces
            	tmp = data[i];  
                data[i] = data[border];
                data[border] = tmp;
            }

                
        }
        i++;
            
    }
    border++;
    if(border != right)
        {
		tmp = data[right];
        data[right] = data[border];
        data[border] = tmp;
        }
    
    return border;
}


template<typename T>
void IntroSortUtil(T data[], T *begin, T *end, int depth)
{
    int size = end-begin;
    if (size < 16)
    {
        InsertionSort(data, begin-data, end-data);
        return;
    }

    if(depth == 0)
    {
        //make_heap(begin, end+1); // sortowanie przez kopcowanie z bliblioteki bits/stdc++.h
        //sort_heap(begin, end+1); // niestety działa zbyt wolno żeby zastosować w sortowaniu introspektywnym
        HeapSort(data, size+1);
        return;
    }

    int pivot = Partition(data, begin-data, end-data);
    IntroSortUtil(data, begin , data+pivot-1, depth-1);
    IntroSortUtil(data, data+pivot+1, end, depth-1);
}


template<typename T>
void IntroSort(T data[], T *left, T *right)
{
    int depth = 2*log(right-left);
    IntroSortUtil(data, left, right,depth);
    return;
}



	/************************/
	/*----------------------*/
	/*      MAIN BODY       */
	/*----------------------*/
    /************************/ 

int main()
{
 
	poczatek:  // goto przyspiesza wykonywanie licznych testów

	int length;
    cout << "Podaj wielkość tablic: ";
    cin >> length;
    cin.ignore(100000,'\n');
    
    srand(time(NULL));
    int** tab = new int *[ROZMIAR];
    

    for(int i = 0; i < ROZMIAR; i++)
    {
        tab[i] = new int[length];  // dynamiczna alokacja
    }


    int wybor=0;
    cout << endl << "Jakie tablice do posortowania utworzyć?" << endl;
    cout << "1. Zupełnie losowe" << endl;
    cout << "2. Częściowo posortowane" << endl;
    cout << "3. Posortowane malejąco" << endl;
    cout << "Wybrano opcję: ";
    cin >> wybor;

    while(wybor != 1 && wybor != 2 && wybor != 3)
	{
		cout << "Taka opcja nie istnieje. Wybierz jeszcze raz: ";
		cin >> wybor;
		cout << endl;
	}

    switch(wybor)
    {	
    	case 1: // full losowo
    	{
			FillRandomly(tab, length);
			break;
		}
		
		case 2: // Częściowo posortowana
		{
    		cout << "W jakim procencie tablice mają być wstępnie posortowane?: ";
    		double percentage;
    		cin >> percentage;
    		percentage = percentage/100;
    		cout << endl;           
    		FillRandomly(tab, length);

     		for(int i = 0; i < ROZMIAR; i++)
	 			SortPercent(tab[i], length, percentage);
     		
	 		break;
		}

		case 3: // posortowana malejąco
		{
			FillRandomly(tab, length);
			for(int i = 0; i < ROZMIAR; i++)   // posortowanie
                QuickSort(tab[i], 0, length-1);
			
			for(int i = 0; i < ROZMIAR; i++)   // odwrócenie
                FillReverse(tab[i], length);    
            break;       
		}

 	}


	int wybor_algorytmu=0;
	cout << "Wybierz algorytm sortowania" << endl;
	cout << "1. Quicksort  |  2. Mergesort  |  3. Introsort" << endl;
	cout << "Wybrano: ";
	cin >> wybor_algorytmu;

	while(wybor_algorytmu != 1 && wybor_algorytmu != 2 && wybor_algorytmu != 3)
	{
		cout << "Taka opcja nie istnieje. Wybierz jeszcze raz: ";
		cin >> wybor_algorytmu;
		cout << endl;
	}

	//WyswietlStan(tab, length);    // przed sortowaniem
	high_resolution_clock::time_point p1 = high_resolution_clock::now();  // czas komputera przed sortowaniem


	switch(wybor_algorytmu)
	{
		case 1: // QUICKSORT
		{
		
			for(int i=0; i<ROZMIAR; i++)
			{
				QuickSort(tab[i], 0, length-1);   // length-1 bo tablica indeksuje od 0
			}
			break;
		}
	

		case 2: // MERGESORT
		{
			int* NewArray = new int[length];
			for(int i=0; i<ROZMIAR; i++)
			{
				MergeSort(tab[i], NewArray, 0, length-1);
			}
			break;
		}

		case 3: // INTROSORT
		{

			for(int i=0; i<ROZMIAR; i++)
			{
				IntroSort(tab[i], tab[i], tab[i]+length-1);
			}
			break;
		}


	}




	high_resolution_clock::time_point p2 = high_resolution_clock::now();  // czas komputera po posortowaniu
	WyswietlStan(tab, length);   // posortowane
	duration<double> time_span = duration_cast<duration<double>>(p2 - p1);   // czas sortowania
	cout << "\n" << "Posortowano w czasie: " << time_span.count() << " sekund \n\n";


	/* Zwolnienie pamięci */
	for(int i=0; i<ROZMIAR; i++)
	{
		delete[] tab[i];
	}
	delete[] tab;


	char znak;
	cout << "Chcesz kontynuować testy? (T/N): ";
	cin >> znak;

	if(znak == 'T' || znak == 't')
		goto poczatek;

	return 0;
}