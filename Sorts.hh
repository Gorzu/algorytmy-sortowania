#ifndef Sorts_hh
#define Sorts_hh

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <ratio>
#include <cmath>
//#include <bits/stdc++.h>

#define ROZMIAR 100


template<typename T>  // wymiana wartości
void swap(T *a, T *b);

template<typename T>  // wypisanie zawartości tablic
void WyswietlStan(T data, int length);

template<typename T>  // wygenerowanie pseudolosowej zawartości tablic
void FillRandomly(T data, int length);

template<typename T>  // sortowanie szybkie
void QuickSort(T data[], int left, int right);

template<typename T>
int Partition(T data[], int left, int right);

template<typename T>  // scalanie
void Merge(T data[], T NewArray[], int left, int middle, int right);  

template<typename T>  // sortowanie przez scalanie
void MergeSort(T data[], T NewArray[], int left, int right);   

template<typename T> // sortowanie przez wstawianie
void InsertionSort(T *tab, int leftIndex, int rightIndex);

template<typename T>  // wstepne sortowanie introspektywne
void IntroSortUtil(T data [],T *begin,T *end,int depthLimit);

template<typename T>  // sortowanie introspektywne
void IntroSort(T data[],T *leftIndex,T *rightIndex);

template<typename T> // sortowanie przez kopcowanie
void HeapSort(T data[], int lenght);

template<typename T>  // ustaw na górze kopca
void ConvertHeap(T data[], int length, int x);



#endif